import type { ReceiptItem } from '@/types/ReceiptItem'
import http from './http'
import type { Receipt } from '@/types/Receipt'

type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
  }[]
  userId?: number
}
function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receipDto: ReceiptDto = {
    orderItems: [],
    userId: 0
  }
  receipDto.userId = receipt.userId
  receipDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })
  return http.post('/orders', receipDto)
}

export default { addOrder }
