import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/services/product'
import type { Product } from '@/types/Product'
import { useMessageStore } from './message'

export const useProductStore = defineStore('product', () => {
  const messageStore = useMessageStore()
  const loadingStore = useLoadingStore()
  const products = ref<Product[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0.0,
    type: { id: 1, name: 'drink' },
    image: 'noimage.jpg',
    files: []
  }
  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  async function getProduct(id: number) {
    loadingStore.doLoad()
    const res = await productService.getProduct(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }
  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res = await productService.getProducts()
      products.value = res.data
      loadingStore.finish()
    } catch (error: any) {
      loadingStore.finish()
      messageStore.showMessage(error.message)
    }
  }
  async function saveProduct() {
    try {
      loadingStore.doLoad()
      const product = editedProduct.value
      if (!product.id) {
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product) // Add new
      } else {
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product) // Update
      }

      await getProducts()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)

    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return { products, getProducts, saveProduct, deleteProduct, editedProduct, getProduct, clearForm }
})
