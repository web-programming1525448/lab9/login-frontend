import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useMessageStore } from './message'

export const useUserStore = defineStore('user', () => {
  const messageStore = useMessageStore()
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])
  const initialUser: User & { files: File[] } = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    image: 'noimage.jpg',
    roles: [{ id: 2, name: 'user' }],
    files: []
  }
  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }
  async function getUsers() {
    try {
      loadingStore.doLoad()
      const res = await userService.getUsers()
      users.value = res.data
      loadingStore.finish()
    } catch (error: any) {
      loadingStore.finish()
      messageStore.showMessage(error.message)
    }
  }
  async function saveUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      if (!user.id) {
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.addUser(user) // Add new
      } else {
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.updateUser(user) // Update
      }

      await getUsers()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)

    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }
  return { users, getUsers, saveUser, deleteUser, editedUser, getUser, clearForm }
})
