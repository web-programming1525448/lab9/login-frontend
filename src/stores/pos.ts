import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'
import { useMessageStore } from './message'

export const usePosStore = defineStore('pos', () => {
  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  async function getProductsByType() {
    try {
      loadingStore.doLoad()
      let res = await productService.getProductsByType(1)
      products1.value = res.data
      res = await productService.getProductsByType(2)
      products2.value = res.data
      res = await productService.getProductsByType(3)
      products3.value = res.data
      loadingStore.finish()
    } catch (error: any) {
      loadingStore.finish()
      messageStore.showMessage(error.message)
    }
  }

  return { products1, products2, products3, getProductsByType }
})
